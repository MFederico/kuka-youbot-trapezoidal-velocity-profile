#include "ros/ros.h"
#include <iostream>
#include <cmath>
#include <vector>
#include <memory>

#include <pcl_ros/point_cloud.h>
#include <pcl/PCLPointCloud2.h>
#include <pcl_ros/transforms.h>
#include <pcl/common/common.h>

#include "geometry_msgs/Vector3.h"


const double offsetAcc = 0.1;
const double wheelRadius = 0.0475; //Youbot wheel radius [m]
const double YoubotMaxVel = 0.8; //Youbot max speed [m/s]

double inline metSecToRadSec(const double& metSec){
	return metSec / wheelRadius; 
}


//Calculate acceleration and time to reach cruise speed
void calculateVariables(const double& qi, const double& qf, const double& tf, double& acc, double& tc){
	acc = ( (4.0 * abs(qf-qi) )  / (tf*tf) ) + offsetAcc;
	tc = ( tf/2.0 ) - (sqrt( ( (tf*tf*acc - 4.0*( qf - qi)) )/ acc )) / 2.0;

	//If cruise speed > YoubotMaxVel throw an exception
	if( acc*tc > YoubotMaxVel){
		throw "Cruise speed greater than 0.8 [m/s], please use different parameters";
	}

	
}
//Publish velocity commands at rate r
void publishTrajectory(std::unique_ptr<std::vector<geometry_msgs::Vector3>>& q, const int r){

	ros::NodeHandle n;
	ros::Rate rate(r);

	ros::Publisher vel_pub = n.advertise<geometry_msgs::Vector3>("/velocity_cmd", 1000);

	//wait for a subscriber
	while(vel_pub.getNumSubscribers() == 0){
		rate.sleep();
	}
	
	geometry_msgs::Vector3 msg;

	for (int i = 0; i< q->size(); i++){
		vel_pub.publish(q->at(i));
		rate.sleep();
  	}

}


void generateTrajectory(std::unique_ptr<std::vector<geometry_msgs::Vector3>>& q, const double& interval, const double& acc, const double& tc, const double& tf){

	const double tCruiseEnd = tf - tc;
	const double increment = metSecToRadSec(acc * interval);
	double currentSpeed = 0.0;
	double currentTime = 0.0;
	const double cruiseSpeed = metSecToRadSec(acc * tc);

	geometry_msgs::Vector3 msg_vel;
	msg_vel.x = 0.0;
	msg_vel.y = 0.0;
	msg_vel.z = 0.0;

    q->push_back(msg_vel);
 
	while(currentTime < tc && currentSpeed < cruiseSpeed){ //Uniform acceleration phase
		currentSpeed += increment;
		currentTime += interval;

		if(currentSpeed > cruiseSpeed){
			currentSpeed = cruiseSpeed;
		}		

		msg_vel.x = currentSpeed;
		q->push_back(msg_vel);
		
	}

	msg_vel.x = cruiseSpeed;
	while(currentTime < tCruiseEnd){ //Cruise speed phase
		currentTime += interval;
		q->push_back(msg_vel);
		
	}

	
	while(currentTime < tf && currentSpeed > 0.0 ){ //Uniform deceleration phase
		currentSpeed -= increment;
		currentTime += interval;

		if(currentSpeed < 0.2){ //0.2 = Youbot minimum wheel speed
			currentSpeed = 0.0;
		}		

		if(currentTime < tf){
			msg_vel.x = currentSpeed;
			q->push_back(msg_vel);
		}
		else{
			msg_vel.x = 0.0;
			q->push_back(msg_vel);
		}
		
	}	
	msg_vel.x = 0.0;
	q->push_back(msg_vel);

}

int main(int argc, char **argv)
{
  ros::init(argc,argv,"planner_camera");
  std::unique_ptr<std::vector<geometry_msgs::Vector3>> q = std::make_unique<std::vector<geometry_msgs::Vector3>>();

  const std::string cld_topic_filtered = "/cloud_filtered";
  const std::string cld_topic_name = "/camera/depth/image";
  const std::string optical_frame = "camera_depth_optical_frame";
  const std::string fixed_frame = "world";


  ros::NodeHandle n;
  boost::shared_ptr<sensor_msgs::PointCloud2 const> shared_pcd;  
  sensor_msgs::PointCloud2 cloud_msg;
  //Get a single message from the cld_topic_filtered topic
  shared_pcd = ros::topic::waitForMessage<sensor_msgs::PointCloud2>(cld_topic_filtered,n);
  cloud_msg = *shared_pcd;
  pcl::PCLPointCloud2 pcl_pc2;             //struttura pc2 di pcl
  pcl_conversions::toPCL(cloud_msg, pcl_pc2); //conversione a pcl della pc2

  pcl::PointCloud<pcl::PointXYZRGB>::Ptr cloud(new pcl::PointCloud<pcl::PointXYZRGB>);
  pcl::PointCloud<pcl::PointXYZRGB>::Ptr camera_frame_cld(new pcl::PointCloud<pcl::PointXYZRGB>);
  pcl::fromPCLPointCloud2(pcl_pc2, *cloud);

  tf::TransformListener listener;
  tf::StampedTransform  world_to_camera;
  double min_sphere_dist;

  try{

	  /*listener.waitForTransform(fixed_frame, optical_frame, cloud_msg.header.stamp, ros::Duration(1.0));
	  listener.lookupTransform(optical_frame, fixed_frame, cloud_msg.header.stamp, world_to_camera);*/

  	  //Wait for transforms to be published on /tf topic
  	  ros::Duration(1.0).sleep();

  	  //Transform from fixed_frame to optical_frame using latest available transform
	  pcl_ros::transformPointCloud(optical_frame, ros::Time(0), *cloud, fixed_frame, *camera_frame_cld, listener);
	  camera_frame_cld->header.frame_id = optical_frame;

	  pcl::PointXYZRGB min_pt, max_pt; 
	  pcl::getMinMax3D(*camera_frame_cld, min_pt, max_pt);

	  //Get the z coordinate (horizontal distance in camera frame)
	  //of the sphere's point which is closer to the camera
	  min_sphere_dist = min_pt.z;

	  //std::cout<<min_sphere_dist<<std::endl;
  }
  catch (tf::TransformException ex)
        {
            ROS_ERROR("%s", ex.what());
        }


  const int rate = 20; //Publishing rate [Hz]
  const double qi = 0.0; //Initial position [m]
  //Get distance of the object exactly in front of the robot (0°)
  const double qf = min_sphere_dist; //Goal position [m]
  const double tf = qf*2.5; //Travel time [s]
  double acc; //Acceleration [m/s^2]
  double tc; //Time needed to reach cruise speed [s]

  //Get acc and tc values
  try{
  	calculateVariables(qi, qf, tf, acc, tc);
  }
  catch (const char* msg) {
  	ROS_ERROR("%s", msg);
  }
  
  generateTrajectory(q, 1.0/rate, acc, tc, tf);
  publishTrajectory(q, rate);
  
  return 0;
}
