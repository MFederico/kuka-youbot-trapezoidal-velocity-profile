#include "ros/ros.h"
#include <iostream>
#include <pcl_ros/point_cloud.h>
#include <pcl/PCLPointCloud2.h>
#include <pcl_ros/transforms.h>
#include <pcl/filters/passthrough.h>
#include <pcl/filters/statistical_outlier_removal.h>
#include <pcl/common/common.h>
#include <math.h>  

#include <tf/transform_listener.h>

#include<sensor_msgs/PointCloud2.h>


pcl::PointCloud<pcl::PointXYZRGB>::Ptr world_frame_cld;

const std::string cld_topic_filtered = "/cloud_filtered";
const std::string cld_topic_name = "/camera/depth/image";
const std::string optical_frame = "camera_depth_optical_frame";
const std::string fixed_frame = "world";

//Camera far clipping plane [m]
const float farClippingPlane = 3.5; 

bool spherePublished = false;


//Add to the cloud a empty sphere centered in c
void makeSphere(pcl::PointCloud<pcl::PointXYZRGB>& cloud, const float& radius, const Eigen::Vector4f& c){
	pcl::PointXYZRGB p;
	//Color the sphere red
	p.r = 180;

	//Get centroid coordinates
	float cz = c.z();
	float cx = c.x();
	float cy = c.y();

	//Convert from spherical coordinates to cartesian coordinates
    //For performance improvement half of the total point of the surface are used
	for(int phi = 0; phi <= 180; phi += 2){
		for(int theta = 0; theta <= 360; theta += 2 ){
			p.x = cx + (radius * cos(theta*M_PI/180) * sin(phi*M_PI/180));
			p.y = cy + (radius * sin(theta*M_PI/180) * sin(phi*M_PI/180));
			p.z = cz + (radius * cos(phi*M_PI/180)); 
			cloud.push_back(p);
		}
	}
}

void cloudCallback(const sensor_msgs::PointCloud2ConstPtr &input){


    //Publish sphere once
    if(! spherePublished ){

    

		//Mark elements as static to make them persistent between calls
		static tf::TransformListener listener;
		static tf::StampedTransform optical2map;

		static ros::NodeHandle n;
  		static ros::Publisher converted_pub =  n.advertise<sensor_msgs::PointCloud2>(cld_topic_filtered,1000);

        pcl::PCLPointCloud2 pcl_pc2;             //struttura pc2 di pcl
        pcl_conversions::toPCL(*input, pcl_pc2); //conversione a pcl della pc2
        sensor_msgs::PointCloud2 filtered_cloud_msg; //filtered pcd message


		//Imposta datatype nel field RGB
        pcl_pc2.fields[3].datatype = sensor_msgs::PointField::FLOAT32;

        pcl::PointCloud<pcl::PointXYZRGB>::Ptr cloud(new pcl::PointCloud<pcl::PointXYZRGB>);
        pcl::PointCloud<pcl::PointXYZRGB>::Ptr cloud_filtered(new pcl::PointCloud<pcl::PointXYZRGB>);
        pcl::fromPCLPointCloud2(pcl_pc2, *cloud);

        //Remove the points further than the camera far clipping plane, relative to camera_depth_optical_frame
        pcl::PassThrough<pcl::PointXYZRGB> pass_;
        pass_.setFilterFieldName("z");
        pass_.setFilterLimits(0.0, farClippingPlane);
        pass_.setInputCloud(cloud);
        pass_.setKeepOrganized(true);
        pass_.filter(*cloud_filtered);

        try
        {

        	//Wait for a transformation from optical_frame to fixed_frame at time input->header.stamp (the data to be transformed)
        	//don't wait for more than 1s
            listener.waitForTransform(optical_frame, fixed_frame, input->header.stamp, ros::Duration(1.0));

            //Get transform from optical_frame to fixed_frame at time input->header.stamp
            listener.lookupTransform(fixed_frame, optical_frame, input->header.stamp, optical2map);

            // bring everything to FIXED FRAME ( if real camera_link, otherwise world )
            pcl_ros::transformPointCloud(*cloud_filtered, *world_frame_cld, optical2map);
            world_frame_cld->header.frame_id = fixed_frame;
			
           	//Remove the points on the floor (between -1mm and 1mm on z), relative to world frame
            pass_.setFilterFieldName("z");
            pass_.setFilterLimits(-0.001, 0.001);
            pass_.setInputCloud(world_frame_cld);
            pass_.setKeepOrganized(true);
            //Keep points that are outside of the filter limits
            pass_.setNegative(true);
            pass_.filter(*cloud_filtered);



            //Remove noise captured by the camera (sparse points)
			//The resulting cloud_out contains all points of cloud_in that have an average distance to their 50 nearest neighbors that is below the computed threshold
            pcl::StatisticalOutlierRemoval<pcl::PointXYZRGB> sor;
            pcl::PointCloud<pcl::PointXYZRGB>::Ptr temp(new pcl::PointCloud<pcl::PointXYZRGB>);
  			sor.setInputCloud (cloud_filtered);
 			sor.setMeanK (50);
 			sor.setStddevMulThresh (1.0);
 			sor.filter (*temp);

 			cloud_filtered = temp;
            
            //Get the minimum and maximum values on each of the 3 (x-y-z) dimensions in cloud_filtered
            pcl::PointXYZRGB min_pt, max_pt; 
            pcl::getMinMax3D(*cloud_filtered, min_pt, max_pt);

            //Find max length segment between height(z component) and width (x component)
            //and use it as the sphere diameter 
            auto sphereDiameter = std::max((max_pt.z - min_pt.z), (max_pt.x - min_pt.x));

            //Find the cloud's centroid to be used as the sphere's centre
            Eigen::Vector4f c;
            pcl::compute3DCentroid(*cloud_filtered,c);

            //Create sphere around obstacle
            makeSphere(*cloud_filtered, sphereDiameter/2.0f, c);
  			pcl::toROSMsg(*cloud_filtered, filtered_cloud_msg);
            converted_pub.publish(filtered_cloud_msg);
            spherePublished = true;

        }
        catch (tf::TransformException ex)
        {
            ROS_ERROR("%s", ex.what());
        }

        }
    }


int main(int argc, char **argv)
{


  ros::init(argc,argv,"cloud_filter");


  
  world_frame_cld = pcl::PointCloud<pcl::PointXYZRGB>::Ptr(new pcl::PointCloud<pcl::PointXYZRGB>);

  ros::NodeHandle n;

  ros::Subscriber cloud_sub;
  
  cloud_sub = n.subscribe(cld_topic_name, 1, &cloudCallback);

  //Handle callbacks
  ros::spin();

  return 0;
}
