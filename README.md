# Kuka Youbot trapezoidal velocity profile 
This repository contains C++ and ROS code for controlling the Kuka Youbot robot.\
Once an object is detected (either by reading the data collected from the laser scanner or the RGBD camera), the ROS nodes send velocity commands to the robot in order to make it reach the object by following a trapezoidal velocity profile.\
The project was tested using V-REP, a popular simulator used in the robotics community. 

![alt text](trapezoidal_profile.png "Trapezoidal profile")

## Project structure 
In the src folder we can find three source files:
- planner_laser.cpp = ROS node used to read data from the laser scanner and generate a trapezoidal velocity profile
- cloud_filter.cpp = ROS node used to filter the PCD data read from the RGBD camera sensor and publish it on a new topic
- planner_camera.cpp = ROS node used to read the filtered PCD data and generate a trapezoidal velocity profile

In the launch folder we can find three launch files, used to launch the aforementioned nodes.


